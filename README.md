# [Task Studio](https://task-studio.gitlab.io/)

Task Studio is a website that lets you create Tasker profiles from your computer.  
If you have any questions, feel free to hop on [our Discord](https://discord.gg/4pdtrQD)

## Contributing

You can contribute to the project by

- Reporting issues for bugs, missing features, suggestions, etc.
- Fixing issues, adding missing features, etc.
- Spreading the word!

Please let us know if you're going to add a feature that doesn't have an issue
for it.

If you're going to take on an issue, please let us know and we'll assign it to you.

## Development Setup

```bash
git clone https://gitlab.com/task-studio/task-studio.gitlab.io
npm install   # To set up dependencies
npm run serve # To run the site locally
```