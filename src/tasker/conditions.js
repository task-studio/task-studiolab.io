import eventspec from "../assets/specs/event.json";
import statespec from "../assets/specs/state.json";

function events() {
  let categories = {};

  eventspec.forEach((e, i) => {
    if (!(e.category in categories)) categories[e.category] = {};

    categories[e.category][e.name] = "e" + e.code;
  });

  return categories;
}

function states() {
  let categories = {};

  statespec.forEach((e, i) => {
    if (!(e.category in categories)) categories[e.category] = {};

    categories[e.category][e.name] = "s" + e.code;
  });

  return categories;
}

export function getCondition(i) {
  const findIn = i.charAt(0) == "s" ? statespec : eventspec;

  return findIn.find(e => e.code === parseInt(i.substring(1)));
}

export default {
  Events: events(),
  States: states()
};
