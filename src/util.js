function resolveObj(path, obj, separator) {
  if (!separator) separator = ".";

  let properties = Array.isArray(path) ? path : path.split(separator);
  return properties.reduce((prev, curr) => prev && prev[curr], obj);
}

function getTimestamp() {
  // gets the current timestamp
  // https://stackoverflow.com/a/221297
  return +new Date();
}

// because js is an absolutely horrible language
// one that cannot even get string splitting right
// why the fuck would splitting an empty string return an array
// WITH AN EMPTY STRING IN IT
function split(str, sep) {
  return !str ? [] : str.split(sep);
}

function download(filename, text) {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(text)
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

export { resolveObj, getTimestamp, split, download };
