import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import Profile from "./tasker/profile";
import Task from "./tasker/task";

Vue.use(Vuex);

const local = new VuexPersistence({
  storage: window.localStorage
});

export default new Vuex.Store({
  plugins: [local.plugin],
  state: {
    profiles: [],
    tasks: [],
    vars: [],

    _currentProfile: null,
    _currentCondition: null,
    _currentTask: null,
    _currentAction: null
  },
  getters: {
    tasks: state => {
      return state.tasks.filter(function (el) {
        return el != null;
      });
    },
    currentTask: state => {
      return state.tasks[state._currentTask];
    }
  },
  mutations: {
    selectProfile: (state, profileId) => {
      state._currentProfile = profileId;
    },
    selectCondition: (state, profileId) => {
      state._currentCondition = profileId;
    },
    deslectCondition: state => {
      state._currentCondition = null;
    },
    selectTask: (state, taskId) => {
      state._currentTask = taskId;
    },
    deslectTask: state => {
      state._currentTask = null;
      state._currentAction = null;
    },
    selectAction: (state, actionId) => {
      state._currentAction = actionId;
    },
    deslectAction: state => {
      state._currentAction = null;
    },
    orderActions: (state, value) => {
      state.tasks[state._currentTask].actions = value;
    },
    setCurrentProfileName: (state, name) => {
      state.profiles[state._currentProfile].name = name;
    },

    setCurrentProfileTask: (state, payload) => {
      let taskObj = payload.exit ? "exitTask" : "task";

      state.profiles[state._currentProfile][taskObj] = payload.task;
    },

    setCurrentTaskName: (state, name) => {
      state.tasks[state._currentTask].name = name;
    },

    setCurrentTriggerOption: (state, payload) => {
      state.profiles[state._currentProfile].trigger.options[payload.key] =
        payload.value;
    },

    setCurrentConditionOption: (state, payload) => {
      Vue.set(
        state.profiles[state._currentProfile].conditionData,
        payload.key,
        payload.value
      );
    },

    setCurrentActionOption: (state, payload) => {
      Vue.set(
        state.tasks[state._currentTask].actions[state._currentAction].options,
        payload.key,
        payload.value
      );
    },

    setCurrentActionLabel: (state, payload) => {
      state.tasks[state._currentTask].actions[
        state._currentAction
      ].label = payload;
    },

    addAction: (state, actionid) => {
      state.tasks[state._currentTask].actions.push({
        id: actionid,
        label: "",
        options: {}
      });
    },
    removeAction: (state, actionId) => {
      state.tasks[state._currentTask].actions.splice(actionId, 1);
      state._currentAction = null;
    },
    addProfile: (state, conditionid) => {
      state.profiles.push(new Profile(state.profiles.length, conditionid));
    },
    removeProfile: (state, profileId) => {
      state.profiles.splice(profileId, 1);
      state.profiles.forEach((element, index) => {
        element.id = index;
      });
    },
    addTask: state => {
      state.tasks.push(new Task(state.tasks.length));
    },
    removeTask: (state, taskid) => {
      state.profiles.forEach((element, index) => {
        if (element.task == taskid) {
          element.task = null;
        }
        if (element.endtask == taskid) {
          element.endtask = null;
        }
      });
      state.tasks.splice(taskid, 1, null);
      let tasks = state.tasks.filter(function (el) {
        return el != null;
      });
      if (tasks.length > 0) {
        state._currentTask = tasks[0].id;
      } else {
        state._currentTask = null;
      }
    }
  }
});
