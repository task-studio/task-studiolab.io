import { getTimestamp, split, clone } from "../util";
import { getCondition } from "./conditions";
import { js2xml } from "xml-js";
import { getAction } from "./actions";

function exportOption(option, index, value, ref) {
  if (option.type === 0 || option.type === 3) {
    if (!("Int" in ref)) {
      ref.Int = [];
    }

    if (option.type === 3) value = value | 0;

    ref.Int.push({
      _attributes: { sr: `arg${index}`, val: value }
    });
  } else if (option.type === 1) {
    if (!("Str" in ref)) {
      ref.Str = [];
    }

    ref.Str.push({
      _attributes: { sr: `arg${index}`, ve: "3" },
      _text: value
    });
  } else if (option.type === 2) {
    if (!("App" in ref)) {
      ref.App = [];
    }

    ref.App.push({
      _attributes: { sr: `arg${index}` },
      appPkg: { _text: value } // todo does this actually work?
    });
  } else if (option.type === 5) {
    /* do not export */
  } else {
    alert(`Unimplemented type ${option.type}. Option will not be exported!`);
  }
}

function exportCondition(profile) {
  const cond = getCondition(profile.condition);
  const type = profile.condition.charAt(0) == "s" ? "State" : "Event"; // todo are events actually "Event"?

  let ret = {};
  ret[type] = {
    _attributes: { sr: "con0", ve: "2" }, // todo increment con0 for multiple conditions,
    code: { _text: cond.code }
  };

  cond.args.forEach((e, i) => {
    exportOption(e, i, profile.conditionData[e.name], ret[type]);
  });

  return ret;
}

function exportProfile(profile) {
  let data = {
    _attributes: {
      sr: "prof" + profile.id,
      ve: "2"
    },
    cdate: { _text: getTimestamp() },
    edate: { _text: getTimestamp() },
    id: { _text: profile.id },
    mid0: { _text: profile.task },
    nme: { _text: profile.name },

    ...exportCondition(profile)
  };

  if (profile.exitTask !== null) {
    data["mid1"] = { _text: profile.exitTask };
  }

  return data;
}

function exportAction(action) {
  const act = getAction(action.id);
  let data = {
    _attributes: { ve: "7" },
    code: action.id,
    label: action.label
  };

  // todo code duplication
  act.args.forEach((a, i) => {
    exportOption(a, i, action.options[a.name], data);
  });

  return data;
}

function exportTask(task) {
  let taskData = {
    _attributes: { sr: "task" + task.id },
    cdate: { _text: getTimestamp() },
    edate: { _text: getTimestamp() },
    id: { _text: task.id },
    nme: { _text: task.name },
    pri: { _text: "100" }, // priority?
    Action: []
  };

  task.actions.forEach((action, i) => {
    let act = exportAction(action);
    act._attributes.sr = "act" + i;
    taskData.Action.push(act);
  });

  return taskData;
}

export default function(profiles, tasks) {
  let data = {
    _comment: ["Exported using Task Studio", "https://task-studio.gitlab.io/"],
    TaskerData: {
      _attributes: {
        sr: "",
        dvi: "1",
        tv: "studio" // tasker version, apperantly not checked
      },
      Project: {
        _attributes: {
          sr: "proj0",
          ve: "2"
        },
        cdate: { _text: getTimestamp() },
        name: { _text: "Task Studio Export" },
        pids: { _text: "" }, // "registered" project ids
        tids: { _text: "" } // "registered" task ids
      },
      Profile: [],
      Task: []
    }
  };

  // add profile data
  profiles.forEach(profile => {
    data.TaskerData.Profile.push(exportProfile(profile));

    let pidArray = split(data.TaskerData.Project.pids._text, ",");
    pidArray.push(profile.id);
    data.TaskerData.Project.pids = pidArray.join(",");
  });

  // add task data
  tasks.forEach(task => {
    data.TaskerData.Task.push(exportTask(task));

    let tidArray = split(data.TaskerData.Project.tids._text, ",");
    tidArray.push(task.id);
    data.TaskerData.Project.tids = tidArray.join(",");
  });

  return js2xml(data, { compact: true, spaces: 4 });
}
