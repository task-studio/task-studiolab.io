import actionspec from "../assets/specs/action.json";

export function getAction(i) {
  return actionspec.find(a => a.code === i);
}

export default (function() {
  let categories = {};

  actionspec.forEach((a, i) => {
    if (a.category === "") return;
    if (!(a.category in categories)) categories[a.category] = {};

    categories[a.category][a.name] = a.code;
  });

  return categories;
})();
