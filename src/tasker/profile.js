export default class Profile {
  constructor(id, condition) {
    this.id = id;
    this.condition = condition;
  }

  id = 0;
  name = "New Profile";

  condition = null;
  conditionData = {};

  task = null;
  exitTask = null;
}
